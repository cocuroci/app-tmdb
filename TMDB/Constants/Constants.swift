//
//  Constants.swift
//  TMDB
//
//  Created by André Cocuroci on 21/11/18.
//  Copyright © 2018 Cocuroci. All rights reserved.
//

import Foundation

struct Constants {
    private init() {}
    
    static let baseUrl = "https://api.themoviedb.org/3"
    static let apiToken = "39c47359fd7c1c58c80933a0f7b2178f"
}
